let params = [];

$("#setting-data-link").on("click", function (event) {
    event.preventDefault();
    document.title = "Paramètres - Données";
    let settingTab = document.getElementById("tab-setting-data");
    if (!settingTab) {
        $(`<li class="nav-item">
           <a class="nav-link closable" id="tab-setting-data" data-toggle="tab" href="#setting-data-tab" 
              role="tab" aria-controls="setting-data" aria-selected="true">
               Données <i class="fas fa-times close-link"></i>
           </a>
        </li>`).appendTo('#module-tab');
        $.ajax({
            url: '/erp/setting/data/',
            type: 'GET',
            success: function (data) {
                $(`<div class="tab-pane fade" id="setting-data-tab" >${data.html}</div>`)
                    .appendTo('#module-tab-content');
                setClick();
                $('#module-tab li:last-child a').tab('show');
                setNextSettingStep(data.next)
            }
        })
    } else {
        $(settingTab).tab('show');
    }
})

function setBtnSubmitStyle(name, value, btnClass) {
    let collapse = $(`#add-data-${name}`)
    let btnSubmit = $(`#btn-submit-data-${name}`)
    if (btnSubmit.val() !== value) {
        btnSubmit
            .removeClass("btn-primary")
            .removeClass('btn-success')
            .addClass(btnClass)
            .val(value)
        collapse.collapse('show')
    } else {
        if (collapse.attr('class').includes('show')) {
            collapse.collapse('hide')
            btnSubmit.val("")
        }
    }
}

function setBtnSubmitClick(data) {
    $(`#btn-submit-data-${data.name}`).on('click', (event) => {
        event.preventDefault();
        let btnSubmit = $(`#btn-submit-data-${data.name}`)
        let url = `erp/api/setting/data/${data.name}/`
        let form = $(`#form-setting-data-${data.name}-add`)
        let dataForm = getFormData(form, data.name + "-");
        if (data.previous) {
            dataForm.previousVal = $(`#form-setting-data-${data.previous} select`).val()
            dataForm.previous = data.previous
            url += dataForm.previousVal + "/"
        }
        if (btnSubmit.val() === $(`#btn-add-data-${data.name}`).val()) {
            $.ajax({
                url: `erp/api/setting/data/${data.name}/`,
                type: 'POST',
                data: dataForm,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                },
                success: function (newData) {
                    $.toast({
                        title: "Ajouté",
                        content: "Donnée créée: " + newData.data,
                        type: "success",
                        delay: 5000
                    })
                    setSettingStep(url)
                }
            })
        } else if (btnSubmit.val() === $(`#btn-edit-data-${data.name}`).val()) {
            let selector = $(`#${data.on_change}`)
            $.ajax({
                url: `erp/api/setting/data/${data.name}/${selector.val()}/`,
                type: 'PUT',
                data: dataForm,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                },
                success: function (newData) {
                    $.toast({
                        title: "Modifié",
                        content: "Donnée modifiée: " + newData.data,
                        bgColor: "bg-primary",
                        delay: 5000
                    })
                    setSettingStep(url)
                }
            })
        }
    })
}

function setNextSettingStep(next, parent) {
    let url = `erp/api/setting/data/${next}/`
    if (parent) {
        url = url + parent + "/"
    }
    setSettingStep(url)
}

function setSettingStep(url) {
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            if (params.findIndex(item => item.name === data.name) < 0) {
                params.push(data)
            }
            $(`#setting-data-${data.name}`).html(data.html)
            $(`#${data.on_change}`).change(function () {
                let selector = this
                if (selector.value) {
                    if (data.next) {
                        $(`#setting-data-${data.next}`).html(`
                            <div class="d-flex justify-content-center">
                              <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                              </div>
                            </div>
                        `)
                        $.ajax({
                            url: `erp/api/setting/data/${data.next}/${selector.value}/`,
                            type: 'GET',
                            success: function () {
                                setNextSettingStep(data.next, selector.value)
                            }
                        })
                    }
                    setSettingAddForm(data, selector.value, function () {
                        let collapse = $(`#add-data-${data.name}`)
                        if (collapse.attr('class').includes('show')) {
                            setBtnSubmitStyle(data.name, $(`#btn-edit-data-${data.name}`).val(), 'btn-primary')
                        }
                    })
                    let index = params.findIndex(item => item.name === data.name)
                    for (let i = index + 1; i < params.length; i++) {
                        $(`#${params[i].on_change}`).val("")
                    }
                    for (let i = index + 2; i < params.length; i++) {
                        $(`#setting-data-${params[i].name}`).html("")
                    }
                    if (selector.value) {
                        $(`#btn-delete-data-${data.name}`).removeAttr("disabled")
                    }
                } else {
                    $(`#btn-delete-data-${data.name}`).attr("disabled", "")
                    let index = params.findIndex(item => item.name === data.name)
                    for (let i = index + 1; i < params.length; i++) {
                        $(`#setting-data-${params[i].name}`).html("")
                    }
                }
            })
            setSettingAddForm(data)
            setBtnAddEdit(data)
            $(`#btn-delete-data-${data.name}`).on('click', function () {
                let selector = $(`#${data.on_change}`)

                $.ajax({
                    url: `erp/api/setting/data/${data.name}/${selector.val()}`,
                    type: 'DELETE',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                    },
                    success: function (dataDelete) {
                        setSettingStep(url)
                        let index = params.findIndex(item => item.name === data.name)
                        for (let i = index + 1; i < params.length; i++) {
                            $(`#setting-data-${params[i].name}`).html("")
                        }
                        $.toast({
                            title: "Supprimé",
                            content: "Donnée supprimé: " + newData.data,
                            bgColor: "bg-error",
                            delay: 5000
                        })
                    }
                })
            })
        }
    })
}

function setSettingAddForm(data, val, callback) {
    let url = `erp/setting/data/get-add-form/${data.name}/`
    if (val) {
        url += val + "/"
    }
    $.ajax({
        url: url,
        type: 'GET',
        success: function (dataForm) {
            $(`#form-setting-data-${data.name}-add`).html(dataForm.form)
            setBtnSubmitClick(data)
            if (callback) {
                callback()
            }
        }
    })
}

function setBtnAddEdit(data) {
    let btnAdd = $(`#btn-add-data-${data.name}`)
    let btnEdit = $(`#btn-edit-data-${data.name}`)
    btnAdd.on('click', function () {
        resetForm($(`#form-setting-data-${data.name}-add`))
        setBtnSubmitStyle(data.name, this.value, 'btn-success')
    })
    btnEdit.on('click', function () {
        let selector = $(`#${data.on_change}`);
        let value = this.value
        if (selector.val()) {
            setSettingAddForm(data, selector.val(), function () {
                setBtnSubmitStyle(data.name, value, 'btn-primary')
            })
        }
    })
}

function resetForm(form) {
    form.find('input:text, input[type=number], input[type=date], input[type=email], input[type=range], input:password, input:file, select, textarea').val('');
    form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
