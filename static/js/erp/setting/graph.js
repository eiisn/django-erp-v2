$("#setting-graph-link").on("click", function (event) {
    event.preventDefault();
    document.title = "Pramètres - Graphiques";
    let graphSettingTab = document.getElementById("tab-setting-graph");
    if (!graphSettingTab) {
        $(`<li class="nav-item">
           <a class="nav-link closable" id="tab-setting-graph" data-toggle="tab" href="#setting-graph-tab" 
              role="tab" aria-controls="setting-graph" aria-selected="true">
               Graphiques <i class="fas fa-times close-link"></i>
           </a>
        </li>`).appendTo('#module-tab');
        $.ajax({
            url: '/erp/setting/graph/',
            type: 'GET',
            success: function (data) {
                $(`<div class="tab-pane fade" id="setting-graph-tab" >${data.html}</div>`)
                    .appendTo('#module-tab-content');
                setClick();
                $('#module-tab li:last-child a').tab('show');
            }
        })
    } else {
        $(graphSettingTab).tab('show');
    }
})
