$("#planning-link").on("click", function (event) {
    event.preventDefault();
    document.title = "Planning";
    let planningTab = document.getElementById("tab-setting");
    if (!planningTab) {
        $(`<li class="nav-item">
           <a class="nav-link closable" id="tab-planning" data-toggle="tab" href="#planning-tab" 
              role="tab" aria-controls="planning" aria-selected="true">
               Planning <i class="fas fa-times close-link"></i>
           </a>
        </li>`).appendTo('#module-tab');
        $.ajax({
            url: '/erp/planning/',
            type: 'GET',
            success: function (data) {
                $(`<div class="tab-pane fade" id="planning-tab" >${data.html}</div>`).appendTo('#module-tab-content');
                setClick();
                let tab = $('#module-tab li:last-child a')
                tab.on('shown.bs.tab', function (e) {
                    console.log(e)
                    setCalendar()
                })
                tab.tab('show');

            }
        })
    } else {
        $(planningTab).tab('show');
    }
})

function setCalendar() {
    let planningElem = document.getElementById('calendar-planning')
    let calendar = new FullCalendar.Calendar(planningElem, {
        events: function (info, successCallback, failureCallback) {
            $.ajax({
                url: 'erp/planning/events/',
                type: 'GET',
                success: function (data) {
                    successCallback(data.array)
                },
                error: function (err) {
                    failureCallback(err)
                }
            })
        },
        headerToolbar: {
            left: 'prevYear,prev,next,nextYear today',
            center: 'title',
            right: 'dayGridMonth,dayGridWeek,dayGridDay'
        },
        editable: true,
        selectable: true,
        businessHours: true,
        initialDate: new Date(),
        navLinks: true, // can click day/week names to navigate views
        dayMaxEvents: true, // allow "more" link when too many events
    });
    calendar.render()
}
