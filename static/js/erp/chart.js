


$(document).ready(() => {
    $.ajax({
        url: 'erp/chart/get-charts/',
        type: 'GET',
        success: function (data) {
            let charts = data.charts;
            for(let i = 0; i < charts.length; i++) {
                let chartData = charts[i];
                let ctx = document.getElementById(`chart-${chartData.name}`).getContext('2d');
                let chart = new Chart(ctx, {
                    type: chartData.type,
                    data: {
                        labels: chartData.labels,
                        datasets: chartData.datasets
                    },
                    options: {}
                })
            }
        }
    })
})
