from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions, status


class ERPView(APIView):

    http_method_names = ["get", "post", "put", "delete"]
    authentication_classes = [authentication.TokenAuthentication, authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    model = None
    post_serializer = None
    get_serializer = None
    sub = None
    parent = None
    parent_model = None
    
    def __init__(self, *args, **kwargs):
        """
        :param django.db.models.Model model:
        :param post_serializer:
        :param get_serializer:
        :param args:
        :param kwargs:
        """
        super(ERPView, self).__init__(*args, **kwargs)
        assert self.get_serializer is not None
        assert self.post_serializer is not None
        assert self.model is not None

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                model = self.model.objects.get(pk=pk)
                serialize_data = self.get_serializer(model)
            except self.model.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        elif self.sub:
            pk_parent = kwargs.get("pk_%s" % self.parent, None)
            if pk_parent:
                model = self.model.objects.filter(**{
                    "%s__pk" % self.parent: pk_parent
                })
                serialize_data = self.get_serializer(model, many=True)
            else:
                return Response({"data": "Need pk_%s" % self.parent}, status=status.HTTP_400_BAD_REQUEST)
        else:
            model = self.model.objects.all()
            serialize_data = self.get_serializer(model, many=True)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        if self.sub:
            pk_parent = kwargs.get("pk_%s" % self.parent, None)
            if pk_parent:
                parent = self.parent_model.objects.get(pk=pk_parent)
                model = self.post_serializer(data=request.data)
                if model.is_valid():
                    model.save()
                    getattr(parent, self.sub).add(model.instance)
                    return Response({"data": model.data})
            else:
                return Response({"data": "Need pk_%s" % self.parent}, status=status.HTTP_400_BAD_REQUEST)
        else:
            model = self.post_serializer(data=request.data)
            if model.is_valid():
                model.save()
                return Response(model.data, status=status.HTTP_201_CREATED)
        return Response(model.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        pk = request.data.get('id', None)
        if pk:
            try:
                instance = self.model.objects.get(pk=pk)
            except self.model.DoesNotExist:
                return Response({"data": "Not found %s" % pk}, status=status.HTTP_404_NOT_FOUND)
            serializer = self.post_serializer(instance=instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need id"}, status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                self.model.objects.get(pk=pk).delete()
            except self.model.DoesNotExist:
                return Response({"data": "Not found %s" % pk}, status=status.HTTP_404_NOT_FOUND)
            return Response({"data": "Delete"}, status=status.HTTP_200_OK)
        return Response({"data": "Need id"}, status=status.HTTP_404_NOT_FOUND)
