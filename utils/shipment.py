import geopy.distance
from random import randint

from django.contrib.contenttypes.models import ContentType

from erp.models import Notification
from datetime import datetime, timedelta
from inventory_stock.models import ShipmentState, Shipment, Location


def schedule_shipment(sale):
    """
    :param sale.models.Sale.Sale sale:
    :return:
    :rtype: Shipment
    """
    customer = sale.customer
    address = sale.customer.address
    sale_detail = sale.saledetail_set.all()
    products = [sd.product for sd in sale_detail]
    # Generate Custom reference
    reference = "%s" % randint(0, 100000)
    while len(reference) < 6:
        reference = "0" + reference
    # Get available warehouse with product in inventory
    available_warehouse = get_warehouse_with_available_product(products)
    # Get start shipment location
    start_location = get_nearest_location(address, available_warehouse)
    # Get Time of package delivery
    planned_date, missing = get_time_between_to_address(start_location, products, available_warehouse)
    if missing:
        for miss_product in missing.keys():
            notif = Notification.objects.create(**{
                "title": "Rupture de stock %s" % miss_product,
                "content": "Vérifier les Stocks de [%s]. Relancer une production." % miss_product,
                "severity": Notification.DANGER[0],
                "content_type": ContentType.objects.get(model=miss_product._meta.model_name),
                "object_id": miss_product.id
            })
            notif.save()
    # Setup State
    state = ShipmentState.objects.get_or_create(libel="En Préparation")[0]
    shipment = Shipment.objects.create(**{
        "reference": reference,
        "planned_date": planned_date,
        "customer": customer,
        "delivery_address": address,
        "start_location": start_location,
        "state": state
    })
    shipment.save()
    return shipment


def get_warehouse_with_available_product(products):
    """
    :param list of product.models.Product.Product products:
    :return: List of Location whit products in Inventory
    :rtype: list of Location
    """
    available_warehouse = []
    for product in products:
        warehouse = Location.objects.filter(warehouse=True, inventory__inventoryline__product=product,
                                            inventory__inventoryline__quantity__gt=0)
        if warehouse:
            for w in warehouse:
                available_warehouse.append(w)
    return available_warehouse


def get_time_between_to_address(location, products, available_warehouse):
    """
    :param Location location:
    :param list of product.models.Product.Product products:
    :param list of Location available_warehouse:
    :return:
    :rtype: datetime or dict
    """
    base = 3
    missing = {}
    for product in products:
        found_in_location = False
        found_in_other_warehouse = False
        for warehouse in available_warehouse:
            p = Location.objects.filter(pk=warehouse.id, inventory__inventoryline__product=product,
                                        inventory__inventoryline__quantity__gt=0)
            if p:
                if warehouse == location:
                    found_in_location = True
                else:
                    found_in_other_warehouse = True
        if found_in_location:
            continue
        elif found_in_other_warehouse:
            base += 1
        else:
            missing[product] = True

    if missing:
        for _ in missing:
            base += 5

    planned_date = datetime.now() + timedelta(days=base)
    return planned_date, missing


def get_nearest_location(address, targets_location):
    """
    :param erp.models.Address.Address address:
    :param list of Location targets_location:
    :return: Nearest location of address
    :rtype: Location
    """
    distance = float('inf')
    ship_location = None
    for location in targets_location:
        g_distance = geopy.distance.distance(
            (address.latitude, address.longitude),
            (location.address.latitude, location.address.longitude)
        )
        if distance >= g_distance:
            distance = g_distance
            ship_location = location
    return ship_location
