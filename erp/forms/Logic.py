from django import forms

from erp.models import Event, Step, Condition


class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        exclude = ('id',)


class StepForm(forms.ModelForm):

    class Meta:
        model = Step
        exclude = ('id', 'order', 'event')


class ConditionForm(forms.ModelForm):

    class Meta:
        model = Condition
        exclude = ('id', '')
