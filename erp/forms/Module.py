from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from erp.models import SubModule, Line

FORM_VALUE_LINK = {
    'null': None,
    'str': lambda x: forms.CharField(max_length=255, required=x.required),
    'int': lambda x: forms.IntegerField(required=x.required),
    'float': lambda x: forms.FloatField(required=x.required),
    'bool': lambda x: forms.BooleanField(required=x.required),
    'date': lambda x: forms.DateField(required=x.required, widget=forms.TextInput(attrs={"type": "date"})),
    'obj': lambda x: forms.ModelChoiceField(queryset=Line.objects.filter(sub_module=x.ref_sub_module_if_obj_value),
                                            required=x.required)
}


class LineForm(forms.ModelForm):

    def __init__(self, sub_module: SubModule, *args, **kwargs):
        super(LineForm, self).__init__(*args, **kwargs)

        crispy_fields = []
        for field in sub_module.field_set.all():
            if FORM_VALUE_LINK[field.value_type]:
                self.fields[field.name.replace(' ', '-')] = FORM_VALUE_LINK[field.value_type](field)
                attrs = {}
                if field.name in self.initial:
                    attrs["readonly"] = True
                if field.value_type == "date":
                    attrs["type"] = "date"
                crispy_fields.append(Field(field.name.replace(' ', '-'), **attrs))

        self.helper = FormHelper()
        self.helper.layout = Layout(*crispy_fields)

    class Meta:
        model = Line
        exclude = ('id', 'line', 'sub_module', 'field_display')
