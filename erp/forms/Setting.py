from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field as CrispyField, Button
from erp.models import Module, SubModule, Field


class SettingGetModuleForm(forms.Form):
    modules = forms.ModelChoiceField(Module.objects.all())

    def __init__(self, *args, **kwargs):
        super(SettingGetModuleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            CrispyField('modules', css_class="my-1 mr-2 ml-2"),
        )


class SettingGetSubModuleForm(forms.Form):

    def __init__(self, module, *args, **kwargs):
        super(SettingGetSubModuleForm, self).__init__(*args, **kwargs)
        self.fields["sub_modules"] = forms.ModelChoiceField(SubModule.objects.filter(module=module))


class SettingGetFieldForm(forms.Form):

    def __init__(self, sub_module, *args, **kwargs):
        super(SettingGetFieldForm, self).__init__(*args, **kwargs)
        self.fields["field"] = forms.ModelChoiceField(Field.objects.filter(sub_module=sub_module))


class SettingModuleForm(forms.ModelForm):

    class Meta:
        model = Module
        fields = ["name", "icon"]

    def __init__(self, *args, **kwargs):
        super(SettingModuleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            CrispyField('name'),
            CrispyField('icon')
        )


class SettingSubModuleForm(forms.ModelForm):

    class Meta:
        model = SubModule
        fields = ["name", "icon", "linked_module"]


class SettingFieldForm(forms.ModelForm):

    class Meta:
        model = Field
        exclude = ["id", "sub_module"]
