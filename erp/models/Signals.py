from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver, Signal
from erp.models.Data import *
from erp.models.Chart import *
from erp.models.Logic import *

IGNORED = [
    Module, SubModule, Field, Event, Step, Condition, Operator, Chart, ChartLabel, ChartDataset, DatasetValue
]
line_modified = Signal(providing_args=["instance"])
line_totally_created = Signal(providing_args=["instance"])


@receiver(post_delete, sender=Line)
def line_deleted(sender, **kwargs):
    print("Line Deleted", end=" ; ")
    print(sender, end=" ; ")
    print(kwargs)


@receiver(line_totally_created, sender=Line)
def line_created(sender, instance, **kwargs):
    print("Line Created", end=" ; ")
    print(sender, end=" ; ")
    print(instance, end=" ; ")
    print(kwargs)


@receiver(post_save, sender=Value)
@receiver(post_save, sender=StringValue)
@receiver(post_save, sender=BooleanValue)
@receiver(post_save, sender=DateValue)
@receiver(post_save, sender=IntegerValue)
@receiver(post_save, sender=ObjectValue)
@receiver(post_save, sender=FieldValue)
def value(sender, instance, created, **kwargs):
    field = instance.field
    events = Event.objects.filter(field=field, when='s')
    for event in events:
        print(event)


@receiver(line_modified, sender=Line)
def modified_line(sender, instance, **kwargs):
    print("Line Modified", end=" ; ")
    print(sender, end=" ; ")
    print(instance, end=" ; ")
    print(kwargs)


# line_modified.connect(modified_line, Line)
