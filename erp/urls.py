"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from erp.views import *


urlpatterns = [
    # Sub Modules
    path('get-sub-module-index/<str:module_pk>/<str:name>/', module_index, name="get-sub-module-index"),
    path('get-sub-module-index/<str:module_pk>/<str:name>/<pk>/', module_index, name="get-sub-module-index"),
    path('api/sub-module/<str:module_pk>/<str:name>/', APIViewERP.as_view(), name="sub-module-api"),
    path('api/sub-module/<str:module_pk>/<str:name>/<pk>/', APIViewERP.as_view(), name="sub-module-api"),
    path('api/sub-module/<str:module_pk>/<str:name>/<pk>/<pk_s>/', APIViewERP.as_view(), name="sub-module-api"),
    # Charts
    path('chart/get-charts/', get_charts, name="get-charts"),
    # Notifications
    path('close-notif/<int:pk_notif>/', close_notif, name='close-notif'),
    # Planning
    path('planning/', planning, name='planning'),
    path('planning/events/', planning_events, name='planning-events'),
    # Paramètres
    #   Données
    path('setting/data/', setting_data, name='setting-data'),
    path('setting/data/get-add-form/<str:name>/', setting_add_data_form, name="setting-get-add-form"),
    path('setting/data/get-add-form/<str:name>/<pk>/', setting_add_data_form, name="setting-get-add-form"),
    path('api/setting/data/<str:name>/', SettingDataAPI.as_view(), name='setting-api'),
    path('api/setting/data/<str:name>/<pk>/', SettingDataAPI.as_view(), name='setting-api'),
    #   Graphiques
    path('setting/graph/', setting_graph, name="setting-graph"),
    #   Notifications
    path('setting/notif/', setting_notif, name="setting-notif"),
]
