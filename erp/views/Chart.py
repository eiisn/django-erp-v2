from django.http import JsonResponse, HttpResponseForbidden
from erp.models import Chart, Line, IntegerValue, ChartLabel


def get_charts(request):
    if request.is_ajax():
        context = {
            "charts": []
        }
        charts = Chart.objects.all()
        for chart in charts:
            chart_data = {
                "name": chart.name,
                "type": chart.chart_type,
                "labels": list(ChartLabel.objects.filter(chart=chart).values_list('label', flat=True)),
                "datasets": []
            }
            datasets = chart.chartdataset_set.all()
            for dataset in datasets:
                dataset_data = {
                    "label": dataset.label,
                    "data": []
                }
                values = dataset.datasetvalue_set.all()
                for value in values:
                    val = 0
                    if value.value_sub_module:
                        val = Line.objects.filter(sub_module=value.value_sub_module).count()
                    elif value.value_field:
                        val = IntegerValue.objects.filter(field=value.value_field)
                        if val:
                            val = sum([v.value for v in val]) / val.count()
                    dataset_data["data"].append(val)
                chart_data["datasets"].append(dataset_data)
            context["charts"].append(chart_data)
        return JsonResponse(context)
    return HttpResponseForbidden()
