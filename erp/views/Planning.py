from django.http import JsonResponse
from django.template import loader

from erp.models import DateValue


def planning(request):
    html = loader.render_to_string('erp/planning/planning.html')
    return JsonResponse({"html": html})


def planning_events(request):

    events = DateValue.objects.all()
    response = {"array": []}
    sorted_events = {}

    for event in events:
        if event.line not in sorted_events:
            sorted_events[event.line] = {}
        if event.field.start_end_or_none_if_date_value is None:
            sorted_events[event.line]['start'] = event.value.strftime('%Y-%m-%d')
            continue
        elif event.field.start_end_or_none_if_date_value is False:
            sorted_events[event.line]['end'] = event.value.strftime('%Y-%m-%d')
        elif event.field.start_end_or_none_if_date_value is True:
            sorted_events[event.line]['start'] = event.value.strftime('%Y-%m-%d')

    for event, value in sorted_events.items():
        sorted_events[event]['title'] = str(event)
        response['array'].append(value)

    return JsonResponse(response)
