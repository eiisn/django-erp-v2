from django.template import loader
from django.http import HttpResponseForbidden, JsonResponse
from erp.forms import LineForm
from erp.models import SubModule


def module_index(request, module_pk, name: str, pk=None):
    if request.is_ajax():
        order = 1
        targets = []
        columns = [{"data": None}]
        list_columns = [""]
        initial_form_add = {}
        initial_form_update = {}
        ajax_url = "erp/api/sub-module/%s/%s/%s" % (module_pk, name.replace(' ', '-'), "%s/" % pk if pk else "")
        template_table = "generic/index_table.html"
        template_form_add = "generic/module_form_add.html"
        template_form_update = "generic/module_form_update.html"
        sub_module = SubModule.objects.get(name=name.replace('-', ' '), module__pk=module_pk)
        sub_module_name = sub_module.name.replace(' ', '-')
        sub_module_fields = sub_module.field_set.all().order_by('order', 'name')
        sub_module_fields_list_values = []
        for field in sub_module_fields:
            if field.value_type == 'obj':
                attr = field.ref_sub_module_if_obj_value.field_set.filter(display=True).order_by('order').first().name
                sub_module_fields_list_values.append("%s.%s" % (field.name.replace(' ', '-'), attr.replace(' ', '-')))
            else:
                sub_module_fields_list_values.append(field.name.replace(' ', '-'))

        sub_module_linked = list(sub_module.sub_module_linked.values_list('name', flat=True).all())

        # Set boolean fields
        i = 0
        for field in sub_module_fields:
            if field.value_type == 'bool':
                targets.append(i + 1)
            i += 1

        # Check if linked module exist
        if sub_module_linked:
            for i in range(len(sub_module_linked)):
                sub_module_linked[i] = sub_module_linked[i].replace(' ', '-')
            list_columns += [""]
            columns += [{
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            }]

        if sub_module.linked_module:
            sub_module_name += "-%s" % pk
            initial_form_add[sub_module.linked_module.name] = pk
            initial_form_update[sub_module.linked_module.name] = pk

        # Set columns and list columns
        for field, formated in zip(sub_module_fields, sub_module_fields_list_values):
            c = {"data": formated}
            if field.value_type == 'obj':
                c['defaultContent'] = "<i class=\"fas fa-exclamation-triangle\"></i>"
            columns.append(c)
        list_columns += [field.name for field in sub_module_fields]

        # Set Adding and Update Forms
        form_add = loader.render_to_string(template_form_add, {
            'module': sub_module_name,
            'form': LineForm(sub_module, prefix="add", initial=initial_form_add),
        }, request)
        form_update = loader.render_to_string(template_form_update, {
            'module': sub_module_name,
            'form': LineForm(sub_module, prefix="update", initial=initial_form_update),
        }, request)

        # Set addon script
        # script = "/static/js/erp/modules/" + sub_module.name + ".js"
        script = None

        # Set returned JSON data
        data = {
            "module": sub_module_name,
            "module_pk": module_pk,
            "sub_module_linked": sub_module_linked,
            "script": script,
            "html": loader.render_to_string(template_table, {
                "module": sub_module_name,
                "list_columns": list_columns
            }),
            "formAdd": form_add,
            "formUpdate": form_update,
            "options": {
                'ajax': ajax_url,
                'dom': 'Bfrtip',
                "select": {
                    "style": 'os',
                    "selector": 'td'
                },
                "order": [[order, 'asc']],
                'buttons': [
                    {
                        'extend': 'print',
                        'className': "btn btn-secondary",
                        'text': '<i class="fas fa-print"></i>',
                        'exportOptions': {
                            'orthogonal': 'export'
                        }
                    },
                    {
                        'extend': 'pdf',
                        'className': 'btn btn-secondary',
                        'text': '<i class="fas fa-file-pdf"></i>',
                        'exportOptions': {
                            'orthogonal': 'export'
                        }
                    },
                    {
                        'text': '<i class="fas fa-sync"></i>',
                        'className': 'btn btn-secondary',
                        'attr': {'id': '%s-btn-sync' % sub_module_name},
                        'action': True
                    }
                ],
                'columnDefs': [
                    {
                        "render": "",
                        "targets": targets
                    },
                    {
                        "targets": 0,
                        "data": None,
                        "defaultContent": '',
                        "orderable": False,
                        "className": 'select-checkbox'
                    }
                ],
                "columns": columns
            }
        }
        return JsonResponse(data)
    return HttpResponseForbidden()
