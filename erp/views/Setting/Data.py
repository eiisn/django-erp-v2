from django.core.exceptions import FieldDoesNotExist
from django.http import JsonResponse, HttpResponseForbidden
from django.template import loader
from django.urls import reverse
from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_api_key.permissions import HasAPIKey

from erp.forms.Setting import *

ASSOC_PARAM = {
    "module": (
        Module, None, SettingModuleForm, SettingGetModuleForm, "id_modules", "sub_module", None
    ),
    "sub_module": (
        SubModule, Module, SettingSubModuleForm, SettingGetSubModuleForm, "id_sub_modules", "field", "module"
    ),
    "field": (
        Field, SubModule, SettingFieldForm, SettingGetFieldForm, "id_field", None, "sub_module"
    )
}


def setting_data(request):
    params = [key for key in ASSOC_PARAM.keys()]
    html = loader.render_to_string("erp/setting/data/setting.html", {"params": params}, request)
    return JsonResponse({"html": html, "next": "module"})


class SettingDataAPI(APIView):

    http_method_names = ['get', 'post', 'put', 'delete', 'head']
    authentication_classes = [authentication.SessionAuthentication, authentication.TokenAuthentication]
    permission_classes = [HasAPIKey | IsAuthenticated]

    def get(self, request, name, pk=None):
        if request.is_ajax():
            ref = None
            args = [name]
            if pk:
                args.append(pk)
                ref = ASSOC_PARAM[name][1].objects.get(pk=pk)
            form = ASSOC_PARAM[name][2](prefix=name)
            get_form = ASSOC_PARAM[name][3](ref)
            html = loader.render_to_string("erp/setting/data/forms.html", {
                "name": name,
                "get_form": get_form,
                "form": form,
                "url": reverse("setting-api", args=args)
            }, request)
            return JsonResponse({
                "html": html,
                "name": name,
                "on_change": ASSOC_PARAM[name][4],
                "next": ASSOC_PARAM[name][5],
                "previous": ASSOC_PARAM[name][6]
            })
        return HttpResponseForbidden()

    def post(self, request, name):
        if request.is_ajax():
            data = self.get_data(request, name)
            ASSOC_PARAM[name][0].objects.create(**data)
            return JsonResponse({"data": "new"})

    def put(self, request, name, pk):
        if request.is_ajax():
            data = self.get_data(request, name)
            instance = ASSOC_PARAM[name][0].objects.get(pk=pk)
            for key, val in data.items():
                instance.__setattr__(key, val)
            instance.save()
            return JsonResponse({"data": "updated"})

    def delete(self, request, name, pk):
        if request.is_ajax():
            instance = ASSOC_PARAM[name][0].objects.get(pk=pk)
            instance.delete()
            return JsonResponse({"data": "delete"})

    def get_data(self, request, name):
        data = {}
        for key, val in request.data.items():
            try:
                remote_field = ASSOC_PARAM[name][0]._meta.get_field(key).remote_field
            except FieldDoesNotExist:
                remote_field = None
            if val == '':
                val = None
            elif remote_field:
                val = remote_field.model.objects.get(pk=val)
            elif val == "true":
                val = True
            elif val == "false":
                val = False
            data[key] = val
        if data.get('previous', None):
            previous_name = data.pop("previous")
            previous_pk = data.pop("previousVal")
            previous = ASSOC_PARAM[previous_name][0].objects.get(pk=previous_pk)
            data[previous_name] = previous
        return data


def setting_add_data_form(request, name, pk=None):
    if request.is_ajax():
        args = [name]
        instance = None
        if pk:
            args.append(pk)
            instance = ASSOC_PARAM[name][0].objects.get(pk=pk)
        form = ASSOC_PARAM[name][2](instance=instance, prefix=name)
        html = loader.render_to_string('erp/setting/data/form_setting_add.html', {
            "name": name,
            "form": form,
            "url": reverse("setting-get-add-form", args=args)
        })
        return JsonResponse({"form": html})
    return HttpResponseForbidden()
