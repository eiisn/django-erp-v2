from django.contrib import admin
from erp.models import *
# Register your models here.


models = (
    Chart, ChartLabel, ChartDataset, DatasetValue, BooleanValue, IntegerValue, ObjectValue, StringValue,
    FloatValue, DateValue, Module, SubModule, Line, Field, Event, Step, Condition, Operator, Notification
)

for model in models:
    admin.site.register(model)
